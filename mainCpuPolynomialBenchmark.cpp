#include <cstdio>
#include <stdlib.h>
#include <sys/types.h>
#include <fcntl.h>
#include <iostream>       // std::cout
#include <thread>         // std::thread
#include <atomic>  
#include <chrono>

#include <string.h>

#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>

#define TYPE_SINGLE 0
#define TYPE_DOUBLE 1

#ifndef POLYNOMIAL_POWER
  #define POLYNOMIAL_POWER 2
#endif

using Clock=std::chrono::high_resolution_clock;

/* Kernel in assembly */
extern "C" void polevlSingle(const double* data, size_t length);
extern "C" void polevlDouble(const double* data, size_t length);

typedef struct {
  int64_t time;
  int32_t counter;
} result_t;

void runPolynomial(result_t *result, const double* data, int32_t numDataElements, std::atomic<bool> &startRunning, std::atomic<bool> &runtimeReached, int32_t typeToEval,
      int32_t numaNumThreads) {
//  result_t resultx;
//  result_t *result = &resultx;
//  double data[numDataElements];
  while(!startRunning) {
  }
  
  auto startTime = Clock::now();
  int32_t counter = 0;
  if (typeToEval == TYPE_SINGLE) {
    while(!runtimeReached) {
      polevlSingle(data, numDataElements);
      counter++;
    }
  } else {
    while(!runtimeReached) {
      polevlDouble(data, numDataElements);
      counter++;
    }    
  }
  
  //do evaluation
  auto stopTime = Clock::now();
  result->time = std::chrono::duration_cast<std::chrono::nanoseconds>(stopTime - startTime).count();
  result->counter = counter;
}

void writeResults(result_t *results, int32_t numDataElements, double intensity, int32_t numThreads, int64_t runTimeInSec, int32_t typeToEval) {
//  std::cout << "base intensity: " << intensity << std::endl;
//  std::cout << "Thread, Performance (GB/s), Performance (Glop/s), Time (ns), Counter" << std::endl;
  
  double totalGBs = 0.0;
  double totalGflops = 0.0;

  int64_t totalCounter = 0;

  double typeSize = ((typeToEval == TYPE_SINGLE)? sizeof(float) : sizeof(double));
  double jumpElements = 20;
  double instrPerCounter = 20;


  for (int32_t i = 0; i < numThreads; i++) {
    totalCounter += results[i].counter;
    double gflops = (((results[i].counter / jumpElements * (double)numDataElements * instrPerCounter * POLYNOMIAL_POWER ) / 1024.0 / 1024.0 / 1024.0) / runTimeInSec /*results[i].time / 1e-9*/);
    double gbs = (((results[i].counter * (double) numDataElements * typeSize ) / 1000 / 1000 / 1000) / runTimeInSec /*results[i].time / 1e-9*/);

    totalGBs += gbs;
    totalGflops += gflops;

//    std::cout << i << ", " << gbs 
//      << ", " << gflops
//      << ", " << results[i].time << ", " << results[i].counter << std::endl;
  }

  std::cout << "Total Counter: " << totalCounter << std::endl;
  std::cout << "Timer: " << runTimeInSec << std::endl;

  std::cout << "Total Performance (GB/s): " << totalGBs << std::endl;
  std::cout << "Total Perforamnce (Gflop/s): " << totalGflops << std::endl;
  std::cout << "Intensity: " << totalGflops / totalGBs << std::endl; // = instr * polypower * 1024**3 / jumpElements / 1000**3 / type
}

void getUserParameter(int argc, char** argv, int32_t *numThreads, int64_t *maxRunTime, double *defaultDataValue, int32_t *numDataElements,
		      int32_t *typeToEval, int32_t *numaNumThreads) {
  int c;
  
  while (1) {
    //int this_option_optind = optind ? optind : 1;
    int option_index = 0;
    static struct option long_options[] = {
	{"numaNumThreads",     	required_argument, 0, 0  },
	{"maxRunTime",  	required_argument, 0, 1  },
	{"defaultDataValue",  	required_argument, 0, 2  },
	{"numDataElements", 	required_argument, 0, 3  },
	{"typeToEval",  	required_argument, 0, 4  },
	{"numThreads",     	required_argument, 0, 5  },
	{0,        			 0,        0, 0  }
    };

    c = getopt_long(argc, argv, "d:",
	      long_options, &option_index);
    if (c == -1) {
      break;
    }

    switch (c) {
      case 0:
	*numaNumThreads = atoi(optarg);
	std::cout << "numaNumThreads: " << *numaNumThreads << std::endl;
      break;
      case 1:
	*maxRunTime = atoi(optarg) * 1e+6;
	std::cout << "maxRunTime: " << ((*maxRunTime) * 1e-6) << std::endl;
      break;
      case 2:
	*defaultDataValue = atof(optarg);
	std::cout << "defaultDataValue: " << *defaultDataValue << std::endl;
      break;
      case 3:
	*numDataElements = atoi(optarg);
	std::cout << "numDataElements: " << *numDataElements << std::endl;
      break;
      case 4:{
	std::string typeToEvalStr(optarg);
	
	if (typeToEvalStr.compare("single") == 0) {
	  *typeToEval = TYPE_SINGLE;
	} else {
	  *typeToEval = TYPE_DOUBLE;
	}
	std::cout << "typeToEval: " << ((*typeToEval == TYPE_SINGLE) ? "Single" : "Double") << std::endl;}
      break;
      case 5:
	*numThreads = atoi(optarg);
	std::cout << "numThreads: " << *numThreads << std::endl;
      break;
    }
  }
  
  
}

// POLYNOMIAL_POWER must be defined during build time (between 0-15)
int main(int argc, char**argv) {
  int32_t numThreads = 1;
  int64_t maxRunTime;
  double defaultDataValue = 0.0;
  int32_t numDataElements;
  int32_t typeToEval = TYPE_SINGLE;
  int32_t numaNumThreads = 0;
  
  getUserParameter(argc, argv, &numThreads, &maxRunTime, &defaultDataValue, &numDataElements, &typeToEval, &numaNumThreads);
  double intensity = POLYNOMIAL_POWER * 2.0 / (typeToEval == TYPE_SINGLE ? 4 : 8);
  
  double *data[numThreads];
  
  data[0] = (double*) malloc(sizeof(double) * numDataElements);
  for (int32_t j = 0; j < numDataElements; j++) {
    data[0][j] = defaultDataValue;
  }

  for (int32_t i = 1; i < numThreads; i++) {
    data[i] = (double*) malloc(sizeof(double) * numDataElements);
    memcpy(data[i], data[0], sizeof(double) * numDataElements);
  }
  
  std::atomic<bool> startRunning(false);
  std::atomic<bool> runtimeReached(false);
  std::thread threads[numThreads];
  result_t results[numThreads];
  
  
  for (int32_t i = 0; i < numThreads; i++) {
    threads[i] = std::thread(runPolynomial, results +i, (double*) data[i], numDataElements, std::ref(startRunning), std::ref(runtimeReached), typeToEval, numaNumThreads);
  }

 
  usleep(5000000);

  time_t my_time = time(NULL);
  printf("startTime: %s", ctime(&my_time));
  
  auto startTime = Clock::now();
  startRunning.store(true);
  
  usleep(maxRunTime);
  
  runtimeReached.store(true);
  
  for (int32_t i = 0; i < numThreads; i++) {
    threads[i].join();
  }

  auto endTime = Clock::now();
  int64_t totalTimeInSec =  std::chrono::duration_cast<std::chrono::seconds>(endTime - startTime).count();

  my_time = time(NULL);
  printf("endTime: %s", ctime(&my_time));
  writeResults(results, numDataElements, intensity, numThreads, totalTimeInSec, typeToEval);
}
