
outdir=/localdisk/lpromber/cpu_benchmark_results/cpuPolynomial_120
maxRunTime=120


for numDataElements in 10000 100000
do
	for typeToEval in single double
	do
		for numThreads in {1..60..1}
		do
			for likwidScript in likwidMEM_SP likwidCYCLE_ACTIVITY
			do
/localdisk/lpromber/${likwidScript}.sh 30 ${outdir}/cpuPolynomialBenchmark.maxRunTime_${maxRunTime}.numDataElements_${numDataElements}.typeToEval_${typeToEval}.numThreads_${numThreads}.${likwidScript} 140 &

				ending=`echo $likwidScript | awk -F'likwid' '{print $2}'`

				/localdisk/lpromber/cpu_benchmark/cpuPolynomialBenchmark --maxRunTime ${maxRunTime} --numDataElements ${numDataElements} --typeToEval ${typeToEval} --numThreads ${numThreads} > ${outdir}/cpuPolynomialBenchmark.maxRunTime_${maxRunTime}.numDataElements_${numDataElements}.typeToEval_${typeToEval}.numThreads_${numThreads}.txt${ending}
				sleep 30
			done
		done
	done
done
