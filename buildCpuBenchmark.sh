POLY_POWER=10

nasm -f elf64 -DPOLYNOMIAL_POWER=${POLY_POWER} -o polynomial.double.o polynomial.double.asm
nasm -f elf64 -DPOLYNOMIAL_POWER=${POLY_POWER} -o polynomial.single.o polynomial.single.asm

g++ -std=c++11 -O0 -Wall -ggdb -DPOLYNOMIAL_POWER=${POLY_POWER} -lrt -o cpuPolynomialBenchmark mainCpuPolynomialBenchmark.cpp polynomial.double.o polynomial.single.o -I/usr/include -L/lib64/ -lpthread 
